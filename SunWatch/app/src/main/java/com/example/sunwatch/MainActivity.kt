package com.example.sunwatch

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import com.example.sunwatch.fragments.HourglassFragment
import com.example.sunwatch.fragments.SetupFragment
import com.example.sunwatch.presenter.MainPresenter
import com.example.sunwatch.utils.ActivityUtil

class MainActivity : AppCompatActivity(), MainPresenter.Listener {

    private var presenter: MainPresenter? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        presenter = MainPresenter(baseContext, this)

        val setupFragment = SetupFragment()
        setupFragment.setPresenter(presenter)
        ActivityUtil.addFragmentToActivity(supportFragmentManager, setupFragment, R.id.frame_layout)
    }

    /**
     *  MainPresenter.Listener
     */

    override fun onNextButtonPress() {
        val hourglassFragment = HourglassFragment()
        hourglassFragment.setPresenter(presenter)
        ActivityUtil.addFragmentToActivity(supportFragmentManager, hourglassFragment, R.id.frame_layout)
    }

    override fun onErrorInput(message: String) {
        Toast.makeText(baseContext, message, Toast.LENGTH_SHORT).show()
    }

    override fun onTimerFinish() {
        onBackPressed()
    }
}
