package com.example.sunwatch.contract

public interface BaseContract {

    interface View <T : Presenter> {
        fun setPresenter(presenter: T?)
    }

    interface Presenter

}
