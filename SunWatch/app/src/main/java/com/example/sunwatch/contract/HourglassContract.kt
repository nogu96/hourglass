package com.example.sunwatch.contract

interface HourglassContract {

    interface View : BaseContract.View<Presenter> {
        fun updateTime(newTime: String)
    }

    interface Presenter : BaseContract.Presenter {
        fun viewReady(view : View)
        fun rotateButtonPressed()
        fun playButtonPressed()
        fun stopButtonPressed()
        fun viewDestroyed()
    }

}