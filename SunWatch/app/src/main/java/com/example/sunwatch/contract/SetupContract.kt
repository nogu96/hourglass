package com.example.sunwatch.contract

interface SetupContract {

    interface View : BaseContract.View<Presenter> {
        fun setTimeTypesOptions(item: Array<String>)
    }

    interface Presenter : BaseContract.Presenter {
        fun viewReady(view : View)
        fun nextButtonPressed(userTimeSetted : String, position : Int)
    }

}