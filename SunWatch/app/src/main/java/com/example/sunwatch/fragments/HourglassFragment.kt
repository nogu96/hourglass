package com.example.sunwatch.fragments


import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import android.widget.ImageView
import android.widget.TextView

import com.example.sunwatch.R
import com.example.sunwatch.contract.HourglassContract

/**
 * A simple [Fragment] subclass.
 *
 */
class HourglassFragment : Fragment(), HourglassContract.View {

    private var presenter : HourglassContract.Presenter? = null
    private var hourglass : TextView? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.fragment_hourglass, container, false)

        hourglass = view.findViewById(R.id.hourglass_time_display)

        view.findViewById<ImageView>(R.id.hourglass_rotate_button).setOnClickListener(View.OnClickListener {
            presenter?.rotateButtonPressed()
        })
        view.findViewById<ImageView>(R.id.hourglass_play_button).setOnClickListener(View.OnClickListener {
            presenter?.playButtonPressed()
        })
        view.findViewById<ImageView>(R.id.hourglass_stop_button).setOnClickListener(View.OnClickListener {
            presenter?.stopButtonPressed()
        })

        presenter?.viewReady(this)

        return view
    }

    override fun setPresenter(presenter: HourglassContract.Presenter?) {
        this.presenter = presenter
    }

    override fun onDestroy() {
        presenter?.viewDestroyed()
        super.onDestroy()
    }

    /*************************
     * HourglassContract.View
     ************************/

    override fun updateTime(newTime: String) {
        hourglass?.setText(newTime)
    }

}
