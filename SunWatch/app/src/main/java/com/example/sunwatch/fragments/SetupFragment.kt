package com.example.sunwatch.fragments

import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import android.widget.ImageView
import android.widget.NumberPicker

import com.example.sunwatch.R
import com.example.sunwatch.contract.SetupContract

class SetupFragment : Fragment(), SetupContract.View {

    private var presenter : SetupContract.Presenter? = null
    private var picker : NumberPicker? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.fragment_setup, container, false)

        val nextButton = view.findViewById<ImageView>(R.id.next_button)
        val insertedTime = view.findViewById<EditText>(R.id.inserted_time_edit_text)
        picker = view.findViewById<NumberPicker>(R.id.number_picker)

        nextButton.setOnClickListener {
            presenter?.nextButtonPressed(insertedTime.text.toString(), picker!!.value)
        }

        presenter?.viewReady(this)

        return view
    }

    override fun setPresenter(presenter: SetupContract.Presenter?) {
        this.presenter = presenter
    }

    override fun setTimeTypesOptions(item: Array<String>) {
        picker?.minValue = 0
        picker?.maxValue = item.size-1
        picker?.displayedValues = item
    }

}
