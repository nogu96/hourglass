package com.example.sunwatch.presenter

import android.content.Context
import android.os.Build
import android.util.Log
import com.example.sunwatch.R
import com.example.sunwatch.contract.HourglassContract
import com.example.sunwatch.contract.SetupContract
import com.example.sunwatch.utils.CustomVibrator
import com.example.sunwatch.utils.HourglassTimer

class MainPresenter(context: Context, listener: Listener) :
    SetupContract.Presenter,
    HourglassContract.Presenter,
    HourglassTimer.Listener {

    private val TAG: String = "mainPresenter"

    private var setupView: SetupContract.View? = null
    private var listener: Listener? = listener
    private var context: Context? = context
    private val timeTypeArray : Array<String> = arrayOf(
        context.getString(R.string.seconds),
        context.getString(R.string.minutes)
    )
    private val timeOptions : IntArray = intArrayOf(1, 60)

    private var hourglassView : HourglassContract.View? = null
    private var hourglassTimer : HourglassTimer? = null
    private var settedTime: Long = 0

    /**
     * SetupContract.Presenter
     */

    override fun viewReady(view: SetupContract.View) {
        setupView = view
        setupView?.setTimeTypesOptions(timeTypeArray)
    }

    override fun nextButtonPressed(userTimeSetted : String, position: Int) {
        if (userTimeSetted.equals("") || userTimeSetted.equals("0")) {
            listener?.onErrorInput(context!!.getString(R.string.time_setted_alert))
            return
        }
        settedTime = userTimeSetted.toLong() * timeOptions.get(position)
        hourglassTimer = HourglassTimer(settedTime)
        hourglassTimer?.setListener(this)
        Log.d(TAG, userTimeSetted)
        listener?.onNextButtonPress()
    }

    /**
     * HourglassContract.Presenter
     */

    override fun viewReady(view: HourglassContract.View) {
        hourglassView = view
        hourglassView?.updateTime(settedTime.toString())
    }

    override fun rotateButtonPressed() {
        hourglassTimer?.rotate()
    }

    override fun playButtonPressed() {
        hourglassTimer?.play()
    }

    override fun stopButtonPressed() {
        hourglassTimer?.stop()
    }

    override fun viewDestroyed() {
        hourglassTimer?.stop()
    }

    /******************************
     * HourglassTimer.Listener
     *****************************/

    override fun timeFinished() {
        CustomVibrator(context!!).vibrate()
        listener?.onTimerFinish()
    }

    override fun onTick(timeLeft: Float) {
        hourglassView?.updateTime(timeLeft.toString())
    }

    interface Listener {
        fun onNextButtonPress()
        fun onErrorInput(message : String)
        fun onTimerFinish()
    }

}