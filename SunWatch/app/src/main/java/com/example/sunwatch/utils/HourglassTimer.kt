package com.example.sunwatch.utils

import android.os.CountDownTimer

class HourglassTimer(settedTime: Long) {

    private val TAG: String = "hourglassTimer"
    private val ON_TICK: Long = 100
    private val TO_MILIS: Int = 1000

    private val settedTime: Float
    private var listener: Listener?
    private var passedTime: Float
    private var timer: CountDownTimer? = null

    private var isPlaying: Boolean = false

    init {
        listener = null
        this.settedTime = settedTime.toFloat() * TO_MILIS
        passedTime = this.settedTime
        generateTimer(this.settedTime)
    }

    fun setListener(listener: Listener) {
        this.listener = listener
    }

    fun play() {
        if (isPlaying)
            return
        timer?.start()
        isPlaying = true
    }

    fun stop() {
        if (!isPlaying)
            return
        timer?.cancel()
        generateTimer(passedTime)
        isPlaying = false
    }

    fun restart() {
        timer?.cancel()
        generateTimer(settedTime)
        isPlaying = false
    }

    fun rotate() {
        timer?.cancel()
        passedTime = settedTime - passedTime
        generateTimer(passedTime)
        timer?.start()
    }

    interface Listener {
        fun timeFinished()
        fun onTick(timeLeft: Float)
    }

    /*********************
     * Private methods
     ********************/

    private fun generateTimer(time: Float) {
        timer = object:  CountDownTimer((time).toLong(), ON_TICK) {
            override fun onTick(millisUntilFinished: Long) {
                passedTime = passedTime - ON_TICK
                listener?.onTick(passedTime)
            }

            override fun onFinish() {
                listener?.timeFinished()
            }
        }
    }

}